## Installation de flask

```
$ pip install -r requirements.txt
```

## Lancemnt du serveur

```
$ python app.py
```

## Test

```
$ curl localhost:8000
Hello World!
```
